module.exports = {
  projects: [
    '<rootDir>/libs/core-utils',
    '<rootDir>/libs/sol2ts-code-gen',
    '<rootDir>/libs/crypto-utils',
    '<rootDir>/libs/synthereum-ts',
    '<rootDir>/apps/examples',
    '<rootDir>/libs/hardhat-utils',
    '<rootDir>/libs/yield-farming',
    '<rootDir>/libs/atomic-swap',
    '<rootDir>/libs/jrt-investors',
    '<rootDir>/libs/synthereum-config',
    '<rootDir>/libs/meta-tx-lib',
    '<rootDir>/apps/burner-wallet',
  ],
};

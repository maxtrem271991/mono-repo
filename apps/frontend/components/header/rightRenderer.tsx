import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  AccountSummary,
  NotificationType,
  useWindowSize,
  styled,
  Skeleton,
} from '@jarvis-network/ui';
import { Address } from '@jarvis-network/core-utils/dist/eth/address';
import {
  formatWalletAddress,
  usePrettyName,
  useAuth,
  useWeb3,
} from '@jarvis-network/app-toolkit';

import { setTheme } from '@/state/slices/theme';
import {
  setAccountOverviewModalVisible,
  setAuthModalVisible,
} from '@/state/slices/app';
import { avatar } from '@/utils/avatar';
import { useReduxSelector } from '@/state/useReduxSelector';
import { State } from '@/state/initialState';
import { isAppReadySelector } from '@/state/selectors';
import { useExchangeNotifications } from '@/utils/useExchangeNotifications';
import { networkIdToName } from '@jarvis-network/core-utils/dist/eth/networks';

const Container = styled.div<{ hasContent: boolean }>`
  height: 38px;
  width: 310px;

  ${props =>
    props.hasContent
      ? ''
      : `
    border-radius: ${props.theme.borderRadius.m};
    overflow: hidden;
  `}

  @media screen and (max-width: ${props =>
    props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex - 1]}px) {
    width: 185px;
  }
`;

const noop = () => undefined;

const render = (): JSX.Element => {
  const dispatch = useDispatch();
  const { account: address, chainId: networkId } = useWeb3();
  const isApplicationReady = useReduxSelector(isAppReadySelector);
  const { logout } = useAuth();
  const name = usePrettyName((address ?? null) as Address | null);
  const [isSigningOut, setSigningOut] = useState(false);
  const { innerWidth } = useWindowSize();
  const notify = useExchangeNotifications();

  const handleLogIn = () => {
    dispatch(setAuthModalVisible(true));
  };

  const handleLogOut = () => {
    logout();
    setSigningOut(true);
  };

  const handleSetTheme = (theme: State['theme']) => {
    dispatch(setTheme({ theme }));
  };

  const handleAccountOverviewOpen = () => {
    dispatch(setAccountOverviewModalVisible(true));
  };

  useEffect(() => {
    if (isSigningOut) {
      setTimeout(() => {
        setSigningOut(false);

        notify('You have successfully signed out', {
          type: NotificationType.error,
          icon: '👋🏻',
        });
      }, 1000);
    }
  }, [isSigningOut, notify]);

  const links = [
    {
      name: 'Account',
      key: 'Account',
      onClick: handleAccountOverviewOpen,
    },
  ];

  const onHelp = () =>
    window.open('https://help.jarvis.exchange/en/', '_blank', 'noopener');

  const getName = () => {
    if (isSigningOut) {
      return '';
    }

    return name || '';
  };

  const getAddress = () => {
    if (isSigningOut) {
      return 'Signing out...';
    }

    return address ? formatWalletAddress(address) : undefined;
  };

  const image = useMemo(
    () => (address && !isSigningOut ? avatar(address) : undefined),
    [address, isSigningOut],
  );

  const networkProp = networkId ? networkIdToName[networkId as 1] : undefined;

  const content = isApplicationReady ? (
    <AccountSummary
      name={getName()}
      wallet={getAddress()}
      image={image}
      menu={links}
      network={networkProp === 'mainnet' ? 'ethereum' : networkProp}
      contentOnTop={innerWidth <= 1080}
      onLogout={isSigningOut ? noop : handleLogOut}
      onLogin={isSigningOut ? noop : handleLogIn}
      onThemeChange={handleSetTheme}
      onHelp={onHelp}
    />
  ) : null;

  return (
    <Container hasContent={!!content}>
      <Skeleton>{content}</Skeleton>
    </Container>
  );
};

const rightRenderer = { render };
export { rightRenderer };

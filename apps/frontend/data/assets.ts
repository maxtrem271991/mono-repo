import {
  primaryCollateralSymbol,
  ExchangeSynthereumToken,
  synthereumConfig,
  allSyntheticSymbols,
} from '@jarvis-network/synthereum-ts/dist/config';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';

import { SubscriptionPair } from '@/utils/priceFeed';

export interface Asset {
  name: string;
  symbol: ExchangeSynthereumToken;
  pair: SubscriptionPair | null;
  price: FPN | null;
  decimals: number;
  type: 'forex' | 'crypto';
}

export interface AssetPair {
  input: Asset;
  output: Asset;
  name: string;
}

export const PRIMARY_STABLE_COIN_TEXT_SYMBOL = '$';

export const PRIMARY_STABLE_COIN: Asset = {
  name: 'USDC',
  symbol: primaryCollateralSymbol,
  pair: null,
  price: new FPN(1),
  decimals: 6,
  type: 'forex',
};

export interface AssetWithWalletInfo extends Asset {
  stableCoinValue: FPN | null;
  ownedAmount: FPN;
}

// FIXME: Instead of hardcoding the networkId and pool version make them dynamic
const syntheticAssets: Asset[] = Object.values(
  synthereumConfig[1].perVersionConfig.v4.syntheticTokens,
).map(info => ({
  name: info.syntheticName,
  symbol: info.syntheticSymbol,
  pair: info.umaPriceFeedIdentifier,
  price: null,
  decimals: 18,
  type: 'forex',
}));
const syntheticAssetsPolygon: Asset[] = Object.values(
  synthereumConfig[137].perVersionConfig.v4.syntheticTokens,
).map(info => ({
  name: info.syntheticName,
  symbol: info.syntheticSymbol,
  pair: info.umaPriceFeedIdentifier,
  price: null,
  decimals: 18,
  type: 'forex',
}));

export const assets: Asset[] = [PRIMARY_STABLE_COIN, ...syntheticAssets];
export const assetsPolygon: Asset[] = [
  PRIMARY_STABLE_COIN,
  ...syntheticAssetsPolygon,
];

export const polygonOnlyAssets: typeof allSyntheticSymbols[number][] = [
  'jPHP',
  'jSGD',
  'jCAD',
];

import { ThemeNameType } from '@jarvis-network/ui';
import { cache } from '@jarvis-network/app-toolkit';
import { SelfMintingMarketAssets } from '@/state/slices/markets';
import {
  ExchangeSelfMintingToken,
  SupportedSelfMintingPairExact,
} from '@jarvis-network/synthereum-config';
import { StringAmount } from '@jarvis-network/core-utils/dist/base/big-number';
import { PriceFeedSymbols } from '@jarvis-network/synthereum-ts/dist/epics/price-feed';

export interface WalletInfo {
  amount: StringAmount;
}
export type OPType = 'borrow';
export interface State {
  theme: ThemeNameType;
  app: {
    isAuthModalVisible: boolean;
    isUnsupportedNetworkModalVisible: boolean;
    poolingFrequency: number;
    isWindowLoaded: boolean;
    networkId: number;
    agentAddress: string | null;
  };
  markets: {
    filterQuery: string | null;
    manageKey: SupportedSelfMintingPairExact | null;
    list: Partial<SelfMintingMarketAssets>;
  };
  wallet: {
    [key in ExchangeSelfMintingToken]?: WalletInfo;
  };
  prices: {
    [key in PriceFeedSymbols]?: StringAmount;
  };
  transaction: {
    params?: any;
    txHash?: string;
    opType?: OPType | 'cancel' | 'initial';
    receipt?: any;
    valid?: boolean;
    error?: {
      message: string;
    };
  };
  approveTransaction: {
    params?: any;
    txHash?: string;
    opType?: OPType | 'cancel' | 'initial' | 'approval';
    receipt?: any;
    valid?: boolean;
    error?: {
      message: string;
    };
  };
}

export const initialAppState: State = {
  theme: cache.get<ThemeNameType | null>('jarvis/state/theme') || 'light',
  app: {
    isAuthModalVisible: false,
    isUnsupportedNetworkModalVisible: false,
    isWindowLoaded: false,
    poolingFrequency: 5000,
    networkId: 0,
    agentAddress: null,
  },
  markets: {
    filterQuery: null,
    manageKey: null,
    list: {},
  },
  wallet: {},
  prices: {},
  transaction: {
    valid: false,
  },
  approveTransaction: {
    valid: false,
  },
};

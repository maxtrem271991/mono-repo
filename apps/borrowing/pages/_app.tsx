import React from 'react';
import type { AppProps } from 'next/app';

import Head from 'next/head';
import { Provider as StateProvider } from 'react-redux';

import {
  BackgroundPreloader,
  NotificationsProvider,
  styled,
  useIsMounted,
} from '@jarvis-network/ui';

import { AppThemeProvider } from '@/components/AppThemeProvider';
import { useStore } from '@/state/store';

import './_app.scss';
import './_onboard.scss';
import 'react-table/react-table.css';
import {
  AuthFlow,
  useSubjects,
  AuthProvider,
} from '@jarvis-network/app-toolkit';
import { CoreObservablesContextProvider } from '@jarvis-network/app-toolkit/dist/CoreObservablesContext';
import { backgroundList } from '@/data/backgrounds';
import { ServiceSelect } from '@/components/auth/flow/ServiceSelect';
import { Welcome } from '@/components/auth/flow/Welcome';
import { Terms } from '@/components/auth/flow/Terms';
import { setAuthModalVisible } from '@/state/slices/app';

const MainWrapper = styled.div`
  height: 100%;
  width: 100vw;
  background: ${props => props.theme.background.primary};
  color: ${props => props.theme.text.primary};
`;

const SelfMintingApp = ({
  Component,
  pageProps,
}: AppProps): JSX.Element | null => {
  const subjects = useSubjects();

  const store = useStore(pageProps.initialReduxState);

  const isMounted = useIsMounted();
  if (!isMounted) return null;

  return (
    <CoreObservablesContextProvider value={subjects}>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, viewport-fit=cover"
        />
      </Head>
      <StateProvider store={store}>
        <AppThemeProvider>
          <NotificationsProvider>
            <AuthProvider>
              <AuthFlow<typeof store>
                ServiceSelect={ServiceSelect}
                Welcome={Welcome}
                Terms={Terms}
                appName="jarvis-borrowing"
                setAuthModalVisibleAction={setAuthModalVisible}
                updateContext
              />

              <BackgroundPreloader backgrounds={backgroundList} />
              <MainWrapper>
                <Component {...pageProps} />
              </MainWrapper>
            </AuthProvider>
          </NotificationsProvider>
        </AppThemeProvider>
      </StateProvider>
    </CoreObservablesContextProvider>
  );
};

// SelfMintingApp.getInitialProps = async (appContext: AppContext) => {
//   const appProps = await App.getInitialProps(appContext);
//   // initialize redux store on server side
//   const reduxStore = initializeStore(initialAppState);
//   //reduxStore.dispatch(setMarketsList(MockMarkets));

//   appProps.pageProps = {
//     ...appProps.pageProps,
//     initialReduxState: reduxStore.getState(),
//   };

//   return appProps;
// };
export default SelfMintingApp;

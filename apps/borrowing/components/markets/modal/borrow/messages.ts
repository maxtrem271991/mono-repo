export const errors = {
  cce: 'Collateralization cap exceeded',
  bmc: 'Below minimum collateralization',
  mlr: 'Mint limit is reached',
  isf: 'Insufficient Funds',
  cir: 'Collateral Input Required',
  sir: 'Synthetic Input Required',
};

export const errors = {
  cce: 'Collateralization cap exceeded',
  isf: 'Insufficient Funds',
};
